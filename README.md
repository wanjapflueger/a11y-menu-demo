# Just Start

| Input                                                            | Output                                                        |
| ---------------------------------------------------------------- | ------------------------------------------------------------- |
| ![Headaches](./readme/christian-erfurt-sxQz2VfoFBE-unsplash.jpg) | ![No headaches](./readme/bruce-mars-AndE50aaHn4-unsplash.jpg) |
| ![TypeScript](./readme/typescript.png)                           | ![JavaScript](./readme/javascript.png)                        |
| ![SCSS](./readme/scss.png)                                       | ![CSS](./readme/css.png)                                      |

---

## Table of contents

<!-- TOC -->

- [Just Start](#just-start)
  - [Table of contents](#table-of-contents)
  - [Develop](#develop)
  - [Dependencies & Requirements](#dependencies--requirements)
  - [What is new?](#what-is-new)
  - [What it does](#what-it-does)
  - [How it works](#how-it-works)
  - [How good is it?](#how-good-is-it)
  - [Input/Output](#inputoutput)
    - [TypeScript/JavaScript](#typescriptjavascript)
    - [SCSS](#scss)
  - [Know issues](#know-issues)
    - [Code is not being linted automatically](#code-is-not-being-linted-automatically)

<!-- /TOC -->

---

## Develop

```bash
npm start
```

## Dependencies & Requirements

- Works on [🍎](https://www.apple.com/) devices
- [Node.js](https://nodejs.org/en/) should be installed on your system so you can run [npm](https://www.npmjs.com/)

## What is new?

See [changelog.md](changelog.md)

## What it does

- **Watch and compile** _TypeScript_ and _Vanilla JavaScript_ to browser readable _JavaScript_ that even _Microsoft_ products understand. You will no longer see error messages like „Cannot use import statement outside a module” or „Refused to execute script from '*.ts' because its MIME type ('video/mp2t') is not executable.”.

    ```js
    // foo.ts
    export function foo() {
      return 'hello'
    }

    // bar.ts
    import { foo } from './foo'
    foo() // hello

    // → bundle.js
    !function(){"use strict";var o={53:function(o,e){Object.defineProperty(e,"__esModule",{value:!0}),e.foo=void 0,e.foo=function(){return"hello"}}},e={};(function r(t){var n=e[t];if(void 0!==n)return n.exports;var u=e[t]={exports:{}};return o[t](u,u.exports,r),u.exports})(53).foo()}();
    ```

- **Watch and compile** _SCSS_ to _CSS_.

    ```scss
    // _bar.scss
    body {
      background-color: $color-red;
    }

    // foo.scss
    $color-red: blue;
    @import 'bar';

    // → foo.css
    body { background-color: blue; }
    ```

- **Watch and transfer** files from src/static to dist

- **Format and lint** Code automatically before you can even write your commit message

    ```js
    // → before $ npm run cqs:js
        export function
        foo   () {
    return 'hello'}

    // → after $ npm run cqs:js
    export function foo() {
      return 'hello'
    }
    ```

## How it works

- Target Browsers are defined in [.browserslistrc](.browserslistrc)

- Compiling: Babel, Webpack, Node Sass, TypeScript

- Formatting and linting: Stylelint, Eslint, Prettier, Lint-Staged, Husky

## How good is it?

Google Lighthouse results say „400/400”:

![Lighthouse results](./readme/lighthouse-results.jpg)

## Input/Output

### TypeScript/JavaScript

```
src/index.{js,ts} (mixing .js and .ts is ok)

↓

dist/bundle.js
dist/bundle.js.map
```

### SCSS

```
src/index.scss

↓

dist/index.css
dist/index.css.map
```

## Know issues

### Code is not being linted automatically

**Note:** [lint-staged](https://www.npmjs.com/package/lint-staged) only works with [husky](https://www.npmjs.com/package/husky) v4 at this moment.

Run the following command to reinstall [lint-staged](https://www.npmjs.com/package/lint-staged) and [husky](https://www.npmjs.com/package/husky):

```bash
npx mrm lint-staged
```
